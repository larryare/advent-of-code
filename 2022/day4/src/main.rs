//Could have done this with rust intervals much easier

use helper::read_file;
use std::cmp::{max, min};
fn main() {
    let f = read_file(false);
    let mut contained_pairs = 0;
    let mut overlapping_pairs = 0;
    for l in f {
        let line_split = l.split(",");
        let mut work_vec: Vec<std::ops::Range<u32>> = vec![];
        for line in line_split {
            let mut w = line.split("-").map(|num| num.parse::<u32>().unwrap());
            work_vec.push(w.next().unwrap()..w.next().unwrap());
        }

        if work_vec[0].start >= work_vec[1].start && work_vec[0].end <= work_vec[1].end
            || work_vec[1].start >= work_vec[0].start && work_vec[1].end <= work_vec[0].end
        {
            contained_pairs += 1;
        }

        if max(work_vec[0].start, work_vec[1].start) <= min(work_vec[0].end, work_vec[1].end) {
            overlapping_pairs += 1;
        }
    }
    println!("{:?}", contained_pairs);
    println!("{:?}", overlapping_pairs);
}
