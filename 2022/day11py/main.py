from dataclasses import dataclass
import math


@dataclass
class Monkey:
    starting_items: list
    operation: str
    test: int
    if_true: int
    if_false: int
    inspected = 0

    @staticmethod
    def parse(lst: list):
        f = open("input.txt", "r")
        lines = f.read().split("\n\n")
        for line in lines:
            attrs = line.split("\n")
            starting_items = [int(x) for x in attrs[1][18:].split(", ")]
            operation = attrs[2][19:]
            test = int(attrs[3][21:])
            if_true = int(attrs[4][29:])
            if_false = int(attrs[5][30:])
            lst.append(Monkey(starting_items, operation, test, if_true, if_false))

    def inspect(self, monkey_list, part1: bool):
        supermodulo = math.prod(monkey.test for monkey in monkey_list)
        for i, old in enumerate(self.starting_items):
            self.inspected += 1
            old = old % supermodulo if not part1 else old
            new = (
                math.floor(eval(self.operation) / 3) if part1 else eval(self.operation)
            )
            monkey_list[
                self.if_true if new % self.test == 0 else self.if_false
            ].starting_items.append(new)
        self.starting_items = []

    @staticmethod
    def monkey_business(monkey_list):
        return math.prod(
            sorted([monkey.inspected for monkey in monkey_list], reverse=True)[:2]
        )


monkey_list = []
Monkey.parse(monkey_list)
# print(monkey_list)
for i in range(20):
    for monkey in monkey_list:
        monkey.inspect(monkey_list, True)

monkey_business = Monkey.monkey_business(monkey_list)

print(monkey_business)

# idk doesn't work
# for i in range(10000):
#     print(i)
#     for monkey in monkey_list:
#         monkey.inspect(monkey_list, False)

# monkey_business = Monkey.monkey_business(monkey_list)

# print(monkey_business)
