use helper::read_file;
use std::thread;

// enum Hand {
//     Rock,
//     Paper,
//     Scissors,
// }

// enum Outcome {
//     Won,
//     Lost,
//     Draw,
// }

fn calc_game(your_hand: &str, hand: &str) -> u32 {
    if your_hand == "A" && hand == "C" {
        6
    } else if your_hand == "C" && hand == "B" {
        6
    } else if your_hand == "B" && hand == "A" {
        6
    } else if your_hand == hand {
        3
    } else {
        0
    }
}

fn reverse(your_hand: &str, hand: &str) -> Result<String, String> {
    let hand_list = ["A", "B", "C"];
    if your_hand == "A" {
        for hand_mod in hand_list {
            if calc_game(hand_mod, hand) == 0 {
                return Ok(String::from(hand_mod));
            }
        }
    } else if your_hand == "B" {
        for hand_mod in hand_list {
            if calc_game(hand_mod, hand) == 3 {
                return Ok(String::from(hand_mod));
            }
        }
    } else {
        for hand_mod in hand_list {
            if calc_game(hand_mod, hand) == 6 {
                return Ok(String::from(hand_mod));
            }
        }
    }
    return Err("Idk".to_string());
}

// fn calc_game(hand: Hand, your_hand: Hand) -> u32 {
//     if let (Hand::Rock, Hand::Scissors) = (your_hand, hand) {
//         3
//     } else {
//         1
//     }

// match your_hand {
//     Hand::Paper => {match hand {

//     }},
//     Hand::Rock => 3,
//     Hand::Scissors => 3,
// }
// }

fn main() {
    let mut total_score = 0;
    let mut alternate_score = 0;

    let sets = read_file(false);

    for set in sets {
        let shape = set.split_whitespace().collect::<Vec<&str>>();
        let mut your_shape = "";

        match shape[1] {
            "X" => {
                total_score += 1;
                your_shape = "A";
            }
            "Y" => {
                total_score += 2;
                your_shape = "B"
            }
            "Z" => {
                total_score += 3;
                your_shape = "C"
            }
            _ => println!("Played nothing"),
        }
        total_score += calc_game(your_shape, shape[0]);

        let hand_played_mod = reverse(your_shape, shape[0]).unwrap();

        match hand_played_mod.as_str() {
            "A" => {
                alternate_score += 1;
            }
            "B" => {
                alternate_score += 2;
            }
            "C" => {
                alternate_score += 3;
            }
            _ => println!("Played nothing"),
        }
        match shape[1] {
            "X" => {
                alternate_score += 0;
            }
            "Y" => {
                alternate_score += 3;
            }
            "Z" => {
                alternate_score += 6;
            }
            _ => println!("Played nothing"),
        }
    }

    println!("{:?}", total_score);
    println!("{:?}", alternate_score)
}
