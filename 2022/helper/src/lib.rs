use std::fs;

pub fn read_file(demo: bool) -> Vec<String> {
    let file_path: &str;
    if demo {
        file_path = "demo-input.txt"
    } else {
        file_path = "input.txt"
    }

    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    contents.lines().map(str::to_string).collect()
}
