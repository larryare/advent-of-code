use array_tool::{self, vec::Intersect};
use helper::read_file;

static ASCII_LOWER: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
];

fn find_shared_item(compartments: (&str, &str)) -> char {
    let mut ltr = 'a';
    for letter in compartments.0.chars() {
        for letter1 in compartments.1.chars() {
            if letter == letter1 {
                ltr = letter1;
                break;
            }
        }
    }
    return ltr;
}

fn fun_name(shared_items: Vec<char>, priority: Vec<char>) -> usize {
    let mut item_sum: usize = 0;
    for item in shared_items {
        item_sum += priority.iter().position(|&r| r == item).unwrap() + 1;
    }
    item_sum
}

fn find_shared_threes(three: Vec<Vec<char>>) -> char {
    let inter = three[0].intersect(three[2].intersect(three[1].clone()));
    inter[0]
}

fn main() {
    let ascii_higher = ASCII_LOWER.map(|letter| letter.to_ascii_uppercase());
    let priority = [ASCII_LOWER, ascii_higher].concat();

    let f = read_file(false);

    let mut shared_items: Vec<char> = [].to_vec();
    for line in f.clone() {
        let compartments = line.split_at(line.len() / 2);
        let shrd = find_shared_item(compartments);
        shared_items.push(shrd);
    }
    let item_sum = fun_name(shared_items, priority.clone());

    println!("{:?}", item_sum);

    let mut threes: Vec<Vec<Vec<char>>> = vec![];

    let mut i = 0;
    while i < f.len() {
        threes.push(vec![
            f[i].clone().chars().collect::<Vec<char>>(),
            f[i + 1].clone().chars().collect(),
            f[i + 2].clone().chars().collect(),
        ]);
        i += 3;
    }
    let mut three_list: Vec<char> = vec![];
    for three in threes {
        let guh = find_shared_threes(three);
        three_list.push(guh);
    }
    println!("{:?}", fun_name(three_list, priority));
}
