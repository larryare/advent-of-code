use std::collections::HashMap;
use std::fs;

fn main() {
    let mut cd = Vec::new();
    let mut sizes: HashMap<String, i32> = HashMap::new();
    for line in fs::read_to_string("input.txt").unwrap().lines() {
        if line.starts_with("$ cd") {
            let dir = &line[5..];
            if dir == ".." {
                cd.pop();
            } else {
                cd.push(dir);
            }
        } else if line.chars().next().unwrap().is_numeric() {
            for i in 0..cd.len() {
                let dir = cd[0..i + 1].join("/");
                let size = line.split(" ").next().unwrap().parse::<i32>().unwrap();
                let entry = sizes.entry(dir).or_insert(0);
                *entry += size;
            }
        }
    }

    let values: Vec<i32> = sizes.values().cloned().collect();
    let max_value = *values.iter().max().unwrap();

    let result1: i32 = values.iter().filter(|&x| *x <= 100000).sum();
    let result2: &i32 = values
        .iter()
        .filter(|&x| *x >= max_value - 40000000)
        .min()
        .unwrap();

    println!("{} {}", result1, result2);
}
