from dataclasses import dataclass
import math


class Rope:
    def __init__(self):
        self.tail: self.Coord = self.Coord()
        self.head: self.Coord = self.Coord()
        self.visited_lst = []

    @dataclass
    class Coord:
        x: int = 0
        y: int = 0

    def move(self, direction: str, qty: int):
        def move_direction(direction, qty, t_or_h):
            for _ in range(qty):
                match direction:
                    case "U":
                        self.head.y += 1
                        move_tail("y", True)
                    case "D":
                        self.head.y -= 1
                        move_tail("y", False)
                    case "R":
                        self.head.x += 1
                        move_tail("x", True)
                    case "L":
                        self.head.x -= 1
                        move_tail("x", False)

        def vector_distance(p1: self.Coord, p2: self.Coord):
            return math.sqrt(((p1.x - p2.x) ** 2) + ((p1.y - p2.y) ** 2))

        def move_tail(dir, p_m):
            if vector_distance(self.head, self.tail) == 2:
                if p_m:
                    setattr(self.tail, dir, getattr(self.tail, dir) + 1)
                else:
                    setattr(self.tail, dir, getattr(self.tail, dir) - 1)
                self.visited_lst.append((self.tail.x, self.tail.y))
            elif vector_distance(self.head, self.tail) > 2:
                match direction:
                    case "U":
                        self.tail = self.Coord(self.head.x, self.head.y - 1)
                    case "D":
                        self.tail = self.Coord(self.head.x, self.head.y + 1)
                    case "R":
                        self.tail = self.Coord(self.head.x - 1, self.head.y)
                    case "L":
                        self.tail = self.Coord(self.head.x + 1, self.head.y)
                self.visited_lst.append((self.tail.x, self.tail.y))

        move_direction(direction, qty, True)


rope = Rope()
with open("input.txt", "r") as f:
    for line in f.readlines():
        dir, qty = line.split(" ")
        rope.move(dir, int(qty))

print(len(set(rope.visited_lst)))
