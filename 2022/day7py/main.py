from __future__ import annotations
from dataclasses import dataclass


@dataclass
class Dir:
    name: str
    size: int
    parent_dir: Dir


cwd = None
dirs_list: list[Dir] = []

with open("input.txt", "r") as f:
    f = f.readlines()
    for line in f:
        if "$ cd" in line:
            to_dir = line.removeprefix("$ cd ").strip()
            if not to_dir == "..":
                cwd = Dir(to_dir, 0, cwd)
                if line == f[0]:
                    dirs_list.append(cwd)
            else:
                cwd.parent_dir.size += cwd.size
                dirs_list.append(cwd)
                cwd = cwd.parent_dir
        elif "$ ls" in line or "dir" in line:
            pass
        else:
            size = int(line.split(" ")[0])
            cwd.size += size

            if line is f[-1]:
                cwd.parent_dir.size += cwd.size
                dirs_list.append(cwd)


def part1():
    dir_sum = sum(dir.size if dir.size < 100_000 else 0 for dir in dirs_list)
    print(part1.__name__ + " " + str(dir_sum))


def part2():
    TOTAL_SIZE = 70_000_000
    NEEDED_SIZE = 40_000_000
    needed_space = (dirs_list[0].size + NEEDED_SIZE) - TOTAL_SIZE
    dir_to_delete = min(
        dir.size if dir.size >= needed_space else TOTAL_SIZE for dir in dirs_list
    )
    print(part2.__name__ + " " + str(dir_to_delete))


part1()
part2()
