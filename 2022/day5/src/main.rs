use helper::read_file;
use regex::Regex;

fn main() {
    let f = read_file(false);

    let nl = f.iter().position(|r| r.is_empty()).unwrap();
    let crates = &f[0..nl - 1];
    let instructions = &f[nl + 1..];

    let mut chars_vec_r = vec![];
    for cr in crates {
        let mut i = 1;
        let mut chars_vec = vec![];
        while i < cr.len() {
            chars_vec.push(cr.chars().nth(i).unwrap());
            i += 4
        }
        chars_vec_r.push(chars_vec);
    }

    let mut chars_vec_c: Vec<Vec<char>> = vec![];

    for i in 0..chars_vec_r[0].len() {
        let mut col_vec = vec![];
        for row in chars_vec_r.clone() {
            if !row[i].is_whitespace() {
                col_vec.push(row[i]);
            }
        }
        col_vec.reverse();
        chars_vec_c.push(col_vec);
    }

    let rx = Regex::new(r"\d+").unwrap();

    let instructions_vec: Vec<Vec<String>> = instructions
        .iter()
        .map(|_crate| {
            rx.captures_iter(_crate.as_str())
                .map(|caps: regex::Captures| {
                    caps.iter().map(|c| c.unwrap().as_str()).collect::<String>()
                })
                .collect::<Vec<_>>()
        })
        .collect();

    let mut part1_chars_vec_c = chars_vec_c.clone();

    for instruction in instructions_vec.clone() {
        let amt = instruction[0].parse::<usize>().unwrap();
        let from_vec_i = instruction[1].parse::<usize>().unwrap() - 1;
        let to_vec_i = instruction[2].parse::<usize>().unwrap() - 1;

        let cloned_vec = part1_chars_vec_c.clone();

        let mut items = vec![];

        for i in 0..amt {
            items.push(part1_chars_vec_c[from_vec_i].remove(cloned_vec[from_vec_i].len() - 1 - i));
        }

        for item in items {
            part1_chars_vec_c[to_vec_i].push(item);
        }
    }

    // Sorry, I'm too lazy to make this into a function

    for instruction in instructions_vec {
        let amt = instruction[0].parse::<usize>().unwrap();
        let from_vec_i = instruction[1].parse::<usize>().unwrap() - 1;
        let to_vec_i = instruction[2].parse::<usize>().unwrap() - 1;

        let cloned_vec = chars_vec_c.clone();

        let mut items = vec![];
        for i in 0..amt {
            items.push(chars_vec_c[from_vec_i].remove(cloned_vec[from_vec_i].len() - 1 - i));
        }
        items.reverse();
        for item in items {
            chars_vec_c[to_vec_i].push(item);
        }
    }

    fun_name(part1_chars_vec_c);
    fun_name(chars_vec_c);
}

fn fun_name(part1_chars_vec_c: Vec<Vec<char>>) {
    let mut top: String = String::from("");
    for mut col in part1_chars_vec_c {
        col.reverse();
        top.push(col[0]);
    }
    println!("{top}");
}
