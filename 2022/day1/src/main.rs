use std::fs;

static FILE_PATH: &str = "input.txt";

fn main() {
    let contents = fs::read_to_string(FILE_PATH).expect("Should have been able to read the file");

    let splits = contents.split("\n\n");

    let mut elf_list: Vec<u32> = vec![];

    for split in splits {
        let spunk = split.split("\n");
        let mut sum: u32 = 0;
        for spun in spunk {
            sum += spun.parse::<u32>().unwrap();
        }
        elf_list.push(sum);
    }

    println!(
        "Elf with highest calories: {:?} \n",
        elf_list.iter().max().unwrap()
    );

    elf_list.sort();
    let mut three_elves: u32 = 0;
    let elf_list_len = elf_list.len();
    for n in 1..4 {
        three_elves += elf_list[elf_list_len - n];
    }

    println!(
        "Sum of 3 elves who have highest calories: {:?}",
        three_elves
    )
}
