cycles = 1
X = 1
breakpnt = 20
signal_strength = 0
pixels = ""
width = 1


def cycle():
    global cycles
    global breakpnt
    global signal_strength
    global pixels
    global width
    if cycles == breakpnt:
        # print("cycle {}".format(cycles))
        # print("X = {}".format(X))
        breakpnt += 40
        signal_strength += cycles * X
    if cycles == width:
        print(pixels[width : width + 40])
        print("\n")
        width += 40
    cycles += 1


with open("input.txt", "r") as f:
    for line in f.readlines():
        match line[0]:
            case "a":
                num = int(line[5:])
                cycle()
                cycle()
                X += num
            case "n":
                cycle()
        pixels += "#"
    # print(signal_strength)
    print(pixels)
