rows = []

with open("demo-input.txt", "r") as f:
    lines = f.read().strip().split()


def start_end():
    start, end = (), ()
    for i, line in enumerate(lines):
        for j, char in enumerate(line):
            if char == "S":
                start = (i, j)
            if char == "E":
                end = (i, j)
    return start, end


start, end = start_end()


print(start)
print(end)


def check_path():
    def if_positive(x):
        if x > -1 and x < len(lines):
            return x
        else:
            return None

    dirs = [
        lines[start[0]][if_positive(start[1] + 1)] or None,  # right
        lines[start[0]][if_positive(start[1] - 1)] or None,  # left
        lines[if_positive(start[0] - 1)][start[1]] or None,  # up
        lines[if_positive(start[0] + 1)][start[1]] or None,  # down
    ]
    print(dirs)


check_path()
