import math


def parse():
    with open("input.txt", "r") as f:
        return list(
            map(lambda x: list(map(int, x)), (row for row in f.read().split("\n")))
        )


trees = parse()

columns = list(map(list, zip(*trees)))
perim = 0

visible = []
ratings = []
for index, row in enumerate(trees):
    if index == 0 or index == len(trees) - 1:
        perim += len(row)
        continue
    for indexi, tree in enumerate(row):
        if indexi == 0 or indexi == len(trees) - 1:
            perim += 1
            continue
        col = columns[indexi]
        l = row[:indexi]
        u = col[:index]
        r = row[indexi + 1 :]
        d = col[index + 1 :]
        directions = [reversed(l), reversed(u), r, d]
        if tree > max(l) or tree > max(u) or tree > max(r) or tree > max(d):
            visible.append(tree)
        rating = []
        for direction in directions:
            vis_trees = 0
            for n in direction:
                vis_trees += 1
                if n >= tree:
                    break
            rating.append(vis_trees)
        ratings.append(math.prod(rating))

print(len(visible) + perim)
print(max(ratings))
