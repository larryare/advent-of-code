use std::vec;

use helper::read_file;

#[derive(Copy, Clone, Debug, PartialOrd, PartialEq)]
struct Game {
    blue: i32,
    green: i32,
    red: i32,
    idx: i32,
}

impl Default for Game {
    fn default() -> Self {
        Game {
            blue: 0,
            green: 0,
            red: 0,
            idx: 0,
        }
    }
}

fn main() {
    let f = read_file(false);

    let mut games: Vec<Vec<Game>> = vec![];

    f.iter()
        .map(|line| line.split(": ").last().unwrap())
        .map(|line| line.split("; ").collect::<Vec<&str>>())
        .map(|line| {
            line.iter()
                .map(|x| x.split(", ").collect::<Vec<&str>>())
                .collect::<Vec<Vec<&str>>>()
        })
        .enumerate()
        .for_each(|(the_big_fat, x)| {
            let mut gmae: Vec<_> = vec![];
            x.iter().for_each(|the_big| {
                let mut game = Game::default();
                game.idx = the_big_fat.try_into().unwrap();
                the_big.iter().for_each(|the_fat| {
                    if the_fat.contains(&"blue") {
                        game.blue = the_fat
                            .split_ascii_whitespace()
                            .next()
                            .unwrap()
                            .parse()
                            .unwrap();
                    }
                    if the_fat.contains(&"green") {
                        game.green = the_fat
                            .split_ascii_whitespace()
                            .next()
                            .unwrap()
                            .parse()
                            .unwrap();
                    }
                    if the_fat.contains(&"red") {
                        game.red = the_fat
                            .split_ascii_whitespace()
                            .next()
                            .unwrap()
                            .parse()
                            .unwrap();
                    }
                });
                gmae.push(game);
            });
            games.push(gmae.clone());
        });

    //println!("{:?}", games);
    let game_rule = Game {
        red: 12,
        green: 13,
        blue: 14,
        idx: 1000000,
    };

    let ggs: Vec<_> = games
        .clone()
        .into_iter()
        .map(|game| {
            game.clone()
                .into_iter()
                .filter(|play| {
                    play.blue <= game_rule.blue
                        && play.red <= game_rule.red
                        && play.green <= game_rule.green
                })
                .collect::<Vec<Game>>()
        })
        .collect();

    let mut sum = 0;
    for play in ggs.clone() {
        if games[play.last().unwrap().idx as usize] == play {
            sum += play.last().unwrap().idx + 1;
            // println!("{:?}", play)
        }
    }

    // part 2
    let powers: i32 = games
        .into_iter()
        .map(|game| {
            [
                game.clone()
                    .into_iter()
                    .map(|play| play.blue)
                    .max()
                    .unwrap(),
                game.clone().into_iter().map(|play| play.red).max().unwrap(),
                game.into_iter().map(|play| play.green).max().unwrap(),
            ]
        })
        .map(|f| {
            f.iter().fold(1, |acc, &x| acc * x)
        }).sum();

    println!("{:?}", powers);

    println!("{:?}", sum);
}
