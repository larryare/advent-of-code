use helper::read_file;

fn main() {
    let number_words = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];

    let replacement_words: Vec<String> = number_words
        .iter()
        .enumerate()
        .map(|(idx, word)| {
            let mut rep_str = String::new();
            rep_str.push_str(&word);
            rep_str.push_str((idx + 1).to_string().as_str());
            rep_str.push_str(&word);
            rep_str
        })
        .collect();

    let f = read_file(false);

    let mut nums: Vec<Vec<i32>> = vec![];

    for mut line in f.clone() {
        for (idx, word) in number_words.iter().enumerate() {
            line = line.replace(word, replacement_words[idx].as_str());
        }
        let mut numss: Vec<i32> = vec![];
        line.chars().for_each(|char_| {
            if char_.is_numeric() {
                numss.push(char_.to_digit(10).unwrap().try_into().unwrap());
            }
        });
        nums.push(numss);
    }

    let sum: i32 = nums
        .iter()
        .map(|pair| pair[0] * 10 + pair.last().unwrap())
        .sum();

    println!("{:?}", sum)
}
