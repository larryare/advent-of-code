use regex::Regex;
fn main() {
    let f = helper::read_file(false);

    let re = Regex::new(r"\d+").expect("Could not unpack regular expression!");

    let file_len = f.len();
    let line_len = f[0].len();

    let wt = f
        .iter()
        .enumerate()
        .map(|(idx, line)| {
            re.find_iter(line.as_str())
                .map(|m| (m.start(), m.end(), m.as_str(), idx))
                .collect::<Vec<_>>()
        })
        .into_iter()
        .filter(|v| !v.is_empty())
        .collect::<Vec<_>>();

    // println!("{:?}", wt);

    fn is_symbol(ch: char) -> bool {
        ch.is_ascii_punctuation() && ch != ".".chars().last().unwrap()
    }

    let symbols: Vec<_> = f
        .clone()
        .into_iter()
        .enumerate()
        .map(|(lidx, line)| {
            line.chars()
                .enumerate()
                .map(|(kas, tau)| (kas, tau))
                .filter(|(_, blt)| is_symbol(*blt))
                .map(|(negerai, kaaaaaaaaa)| (kaaaaaaaaa, negerai, lidx))
                .collect::<Vec<_>>()
        })
        .filter(|v| !v.is_empty())
        .collect();

    // println!("{:?}", &symbols);
    let mut ze: Vec<_> = vec![];
    wt.into_iter().for_each(|code| {
        code.into_iter().for_each(|v| {
            ze.push(v);
        });
    });

    println!("{:?}", ze.len());

    let mut zs: Vec<_> = vec![];
    symbols.into_iter().for_each(|code| {
        code.into_iter().for_each(|v| {
            zs.push(v);
        });
    });

    println!("{:?}", zs.len());

    let mut sum = 0;
    for code in ze.clone() {
        let start = if code.0 > 0 {code.0 - 1} else {0};
        let end = if code.1 < line_len - 1 {code.1 + 1} else {code.1};
        let cols = if code.3 > 0 {code.3 - 1} else {0};
        let cole = if code.3 < file_len - 1 {code.3 + 1} else {code.3};
        // println!("trying {:?}", code.2);

        for symbol in zs.clone() {
            if symbol.1 <= end && symbol.1 >= start && symbol.2 <= cole && symbol.2 >= cols {
                sum += code.2.parse::<i32>().unwrap();
                // println!("{:?}", code.2);
                break;
            }
        }
    }
    println!("{:?}", sum);
}
