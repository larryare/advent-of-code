use std::collections::btree_map::Range;

struct Map {
    destination: u32,
    source: u32,
    range: usize,
}
fn main() {
    let f = helper::read_file(true);

    let seeds: Vec<_> = f[0].split(":").last().unwrap().split_whitespace().collect();

    let section_headers: Vec<_> = f
        .iter()
        .enumerate()
        .filter_map(|line| {
            fn fuck(line: (usize, &String)) -> Option<usize> {
                if line.1.find(":").is_some() {
                    Some(line.0)
                } else {
                    None
                }
            }
            fuck(line)
        })
        .collect();

    let section_ranges: Vec<_> = section_headers
        .iter()
        .skip(1)
        .zip(section_headers.iter().skip(2))
        .map(|(idx1, idx2)| idx1 + 1..*idx2 - 1)
        .collect();

    let sections: Vec<_> = section_ranges.into_iter().map(|section_idx| {
        // println!("{:?}", section_idx);
            &f[section_idx]
    }).collect();
    

    let s_num: Vec<_> = sections.iter().map(|f| {
        f.iter().map(|g| {
            g.split_whitespace().collect::<Vec<_>>()
        })
    }).collect();

    println!("{:?}", s_num);
}
