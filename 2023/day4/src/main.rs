fn main() {
    let f = helper::read_file(false);

    let fv: Vec<_> = f
        .iter()
        .map(|line| {
            line.split(": ")
                .last()
                .unwrap()
                .split(" | ")
                .collect::<Vec<_>>()
        })
        .map(|v| {
            v.iter()
                .map(|vs| {
                    vs.split_whitespace()
                        .map(|num| num.parse::<i32>().unwrap())
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        })
        .collect();

    let mut common: Vec<i32> = vec![];
    for v in &fv {
        let mut common_elements = Vec::new();

        for &item in v[0].iter() {
            if v[1].contains(&item) {
                common_elements.push(item);
            }
        }
        common.push(common_elements.len().try_into().unwrap());

        // println!("{:?}", common_elements);
        // println!("{:?}", v[0]);
        // println!("{:?}", v[1]);
        // let matching = v[0].iter().zip(&v[1]).filter(|&(a, b)| a == b).count();
        // println!("{}", matching);
    }

    let answers:Vec<_> = common.into_iter().map(|num|{
        if num == 0 {0} else {let n = i32::pow(2, (num - 1).try_into().unwrap()); n}
    }).collect();

    println!("Part 1: {:?}", answers.iter().sum::<i32>());
}
